# Changelog for Matrix Recorder

## Changes in v0.0.2 (2017-02-06)

- Will now display device key (for verification) after initial sync


## Changes in v0.0.1 (2017-01-31)

- Initial release
