# Matrix Recorder
While [Matrix](https://matrix.org/) homeservers store your message history so that you can retrieve them on all your devices, currently there is no easy way to download and backup this message history. This is especially relevant for end-to-end (E2E) encrypted rooms because their history cannot be decrypted anymore if you lose your encryption keys (which is as easy as logging out of your Riot client).

This is where *Matrix Recorder* jumps in. This tool (written in [Node.js](https://nodejs.org/en/)) retrieves all messages you receive or send just as a regular client would. It uses your existing Matrix account but registers as a new device with its own encryption keys, which means that it can decrypt messages sent to you after Matrix Recorder has been registered. You don’t need to keep it running all the time but can just start it from time to time - it will continue from where it left off when you start it the next time.

All timeline events retrieved that way will be stored in an sqlite database which you can just keep as a backup or use to create your own archive. Matrix Recorder comes with a small utility that extracts messages from this database into HTML files that can be viewed in your browser.


## Installation
To use Matrix Recorder, you need to have installed a recent version of [Node.js](https://nodejs.org/en/) (recommended is at least v6.9) including the `npm` package manager. For easy checkout from gitlab you should also have an installation of `git`. With these tools in place, installing Matrix Recorder should be as easy as:

	git clone https://gitlab.com/argit/matrix-recorder.git
	cd matrix-recorder/
	npm install

The last line will install all required dependencies in the `node_modules` subfolder.


## Usage
If Matrix Recorder is installed properly, you can use it by telling it where you want to store your session data and the retrieved message data (this should be an empty or to-be-created directory), for example like this (you need to run it from the folder you installed it in above):

	node matrix-recorder.js /home/you/Chat_Archive

If you run it for the first time with this folder it will ask you for your home server URL (`https://matrix.org/` if you are using the official home server of the Matrix team), your username and password. It will also ask you whether it should go back in time and how many old messages it should retrieve for you - note, though, that it won’t be able to decrypt any old messages with active end-to-end encryption (E2E) as those messages would not have been encrypted for the new keys Matrix Recorder generates.

Matrix Recorder will then login to your homeserver, register as a new device (with a new set of keys), download the number of old messages you specified and continue listening. From then on, any new E2E messages sent to you will by default be encrypted also for your Matrix Recorder keys, so that it can decrypt and store these messages as well.

*Warning:* Yes, Matrix Recorder will decrypt your E2E messages, just like your Matrix client (e.g. Riot) will. You should thus only run Matrix Recorder on a trusted computer (ideally your own). There is no need to keep it running all the time (continue reading below), so it is neither necessary nor advisable to run it on some far away server or cloud machine!

Matrix Recorder will keep listening for new messages for as long as you keep it running. You can always quit it by pressing `Ctrl-C`. This does not mean that it will miss any messages, though - when you restart it with the same target directory it will just continue where it left off and retrieve all new messages that have arrived in the meantime. You do thus not have to keep it running continuously but can just start it from time to time to catch up with your new communications.

Matrix Recorder stores its access token (which it uses to access your home server on your behalf) and its encryption keys in plaintext in the `localstorage/` subfolder of the target directory you have specified. It will also record all messages received in plaintext in the sqlite database `messages.sqlite` in the target directory (after all, that’s the whole point of this tool!). Incoming media (images, video, files) will be decrypted (if necessary) and stored in the `media/` subdirectory. Thus, you might want to consider pointing Matrix Recorder to a directory on an encrypted volume to keep your private data secure.


## Viewing your messages
You can either directly access the `messages.sqlite` database to retrieve your messages, or you can use our `recorder-to-html.js` helper by calling it with the same target directory you have pointed Matrix Recorder at:

	node recorder-to-html.js /home/you/Chat_Archive

This tool will create a new subfolder `html/` in the target directory. It will then go through the database and extract all messages into HTML files, which it will put in there. After the tool has completed, you can open the `html/index.html` file in a browser of your choice and you should be able to browse the stored room history. Feel free to modify the `style.css` stylesheet to customise the look to your taste (and if it looks great please send us a pull request!).

If new messages have come in and you want to update the HTML view you first need to remove the `html/` subfolder (because we do not want to overwrite anything by accident) and afterwards you can just call the above command again.


## Current Limitations and Contributions
- It’s alpha code - don’t use it for important archiving tasks.
- It doesn’t support any other homeserver authentication method than username and password.
- It cannot decrypt E2E messages that have been sent prior to your first run of Matrix Recorder (this is by design as obviously those messages won’t have been encrypted for Matrix Recorder, as an alternative it would have to be able to import your private keys e.g. from your Riot client, which is not supported yet).
- The `recorder-to-html.js` script currently will only process a very basic subset of message types - join, leave and invite membership events plus regular text messages, images, videos and generic file uploads.

Feel free to enhance the code, we are looking forward to any pull requests for Matrix Recorder.


## Questions?
If you have any questions, feel free to join [\#hello-matrix:matrix.org](https://matrix.to/#/#hello-matrix:matrix.org) for answers. If any questions come up frequently, we will add them here.


